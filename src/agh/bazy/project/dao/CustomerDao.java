package agh.bazy.project.dao;

import agh.bazy.project.model.Customer;
import agh.bazy.project.model.User;
import agh.bazy.project.util.SessionManager;
import agh.bazy.project.util.UserType;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public class CustomerDao extends GenericDao<Customer> {
    public Optional<Customer> createCustomer(String username, String password, String companyName, String city, String street, String zipCode,
                                             double discount){
        Customer customer = new Customer();
        customer.CompanyName = companyName;
        customer.City = city;
        customer.Street = street;
        customer.ZipCode = zipCode;
        customer.discount = discount;
        customer.Invoices = new HashSet<>();

        UserDao userDao = new UserDao();
        Optional<User> user = userDao.createUser(username,password,UserType.CUSTOMER, customer);
        if(user.isPresent()) {
            customer.User = user.get();
            try {
                save(customer);
            } catch (Exception e) {
                e.printStackTrace();
                return Optional.empty();
            }
            return Optional.of(customer);
        }
        return Optional.empty();
    }

    public List<Customer> getAllCustomers(){
        return SessionManager.getSession().createQuery("SELECT c FROM Customer c").getResultList();
    }

    public Customer getByLogin(String login){
        return (Customer)SessionManager.getSession().createQuery("SELECT c FROM Customer c WHERE c.User.Username = :username")
                .setParameter("username", login).getSingleResult();
    }
}
