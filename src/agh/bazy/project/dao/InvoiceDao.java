package agh.bazy.project.dao;

import agh.bazy.project.model.Category;
import agh.bazy.project.model.Customer;
import agh.bazy.project.model.Invoice;
import agh.bazy.project.model.InvoiceSlot;
import agh.bazy.project.util.SessionManager;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public class InvoiceDao extends GenericDao<Invoice> {

    public Optional<Invoice> createInvoice(Customer customer, List<InvoiceSlot> slots) {
        Invoice invoice = new Invoice();
        invoice.Customer = customer;
        invoice.InvoiceSlots = new HashSet<>();
        for (InvoiceSlot is : slots) {
            invoice.InvoiceSlots.add(is);
            is.Invoice = invoice;
            is.Product.UnitsOnStock -= is.Quantity;
            SessionManager.getSession().save(is.Product);
        }

        try {
            for (InvoiceSlot is : slots) {
                SessionManager.getSession().save(is);
            }
            save(invoice);
            customer.Invoices.add(invoice);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
        return Optional.of(invoice);
    }

    public Optional<Invoice> getInvoiceById(int id){
        Invoice invoice;
        try {
            invoice = (Invoice) SessionManager.getSession()
                    .createQuery("SELECT i FROM Invoice i WHERE i.InvoiceNumber = :id")
                    .setParameter("id", id).getSingleResult();
        }catch (Exception e){
            e.printStackTrace();
            return Optional.empty();
        }
        return Optional.of(invoice);
    }
}
