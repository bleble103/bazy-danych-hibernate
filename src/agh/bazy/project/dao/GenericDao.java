package agh.bazy.project.dao;

import agh.bazy.project.util.SessionManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class GenericDao <T> {
    public void save(T object){
        Session session = SessionManager.getSession();
        Transaction t = session.beginTransaction();

        session.save(object);
        t.commit();
    }
    public void delete(T object){
        Session session = SessionManager.getSession();
        Transaction t = session.beginTransaction();

        session.delete(object);

        t.commit();
    }
}
