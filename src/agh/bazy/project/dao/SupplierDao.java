package agh.bazy.project.dao;

import agh.bazy.project.model.Customer;
import agh.bazy.project.model.Supplier;
import agh.bazy.project.model.User;
import agh.bazy.project.util.SessionManager;
import agh.bazy.project.util.UserType;

import java.util.List;
import java.util.Optional;

public class SupplierDao extends GenericDao<Supplier> {
    public Optional<Supplier> createSupplier(String username, String password, String companyName, String city, String street, String zipCode,
                                             String accountNumber){
        Supplier supplier = new Supplier();
        supplier.CompanyName = companyName;
        supplier.City = city;
        supplier.Street = street;
        supplier.ZipCode = zipCode;
        supplier.BankAccountNumber = accountNumber;

        UserDao userDao = new UserDao();
        Optional<User> user = userDao.createUser(username,password,UserType.SUPPLIER, supplier);
        if(user.isPresent()) {
            supplier.User = user.get();
            try {
                save(supplier);
            } catch (Exception e) {
                return Optional.empty();
            }
            return Optional.of(supplier);
        }
        return Optional.empty();
    }

    public List<Supplier> getAllSuppliers(){
        return SessionManager.getSession().createQuery("SELECT s FROM Supplier s").getResultList();
    }
}
