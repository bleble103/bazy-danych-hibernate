package agh.bazy.project.dao;

import agh.bazy.project.model.Category;
import agh.bazy.project.model.Company;
import agh.bazy.project.model.Product;
import agh.bazy.project.model.User;
import agh.bazy.project.util.SessionManager;
import agh.bazy.project.util.UserType;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public class ProductDao extends GenericDao<Product> {

    public Optional<Product> createProduct(String productName, Category category, int unitsInStock){
        Product product = new Product();
        product.category = category;
        product.ProductName = productName;
        product.UnitsOnStock = unitsInStock;

        try{
            save(product);
            category.Products.add(product);
        } catch (Exception e){
            return Optional.empty();
        }
        return Optional.of(product);
    }

    public Optional<Product> getById(int id){
        Product prod;
        try {
            prod = (Product) SessionManager.getSession()
                    .createQuery("SELECT p FROM Product p WHERE p.id = :id")
                    .setParameter("id", id).getSingleResult();
        }catch (Exception e){
            e.printStackTrace();
            return Optional.empty();
        }
        return Optional.of(prod);
    }

}
