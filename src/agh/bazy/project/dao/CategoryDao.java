package agh.bazy.project.dao;

import agh.bazy.project.model.Category;
import agh.bazy.project.model.Company;
import agh.bazy.project.model.User;
import agh.bazy.project.util.SessionManager;
import agh.bazy.project.util.UserType;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public class CategoryDao extends GenericDao<Category> {
    public Optional<Category> createCategory(String categoryName){
        Category category = new Category();
        category.Name = categoryName;
        category.Products = new HashSet<>();

        try{
            save(category);
        } catch (Exception e){
            return Optional.empty();
        }
        return Optional.of(category);
    }

    public List<Category> getAllCategories(){
        return SessionManager.getSession().createQuery("SELECT c FROM Category c").getResultList();
    }

    public Optional<Category> getById(int id){
        Category cat;
        try {
            cat = (Category)SessionManager.getSession().createQuery("SELECT c FROM Category c WHERE c.CategoryID = :id")
                    .setParameter("id", id).getSingleResult();
        }catch (Exception e){
            e.printStackTrace();
            return Optional.empty();
        }
        return Optional.of(cat);
    }
}
