package agh.bazy.project.dao;

import agh.bazy.project.model.Company;
import agh.bazy.project.model.User;
import agh.bazy.project.util.SessionManager;
import agh.bazy.project.util.UserType;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public class UserDao extends GenericDao<User> {
    public Optional<User> createUser(String username, String password,
                                     UserType userType, Company company){
        User user = new User();
        user.Username = username;
        user.Password = password;
        user.UserType = userType;
        user.Company = company;

        try{
            save(user);
        } catch (Exception e){
            e.printStackTrace();
            return Optional.empty();
        }
        return Optional.of(user);
    }

    public Optional<User> findByUsername(String username){
        Query query = SessionManager.getSession()
                .createQuery("SELECT u FROM User u WHERE u.Username = :username")
                .setParameter("username", username);
        User user;
        try {
            user = (User) query.getSingleResult();
        }
        catch (Exception e){
            e.printStackTrace();
            return Optional.empty();
        }
        return Optional.of(user);
    }

    public List<User> getAllUsers(){
        return SessionManager.getSession()
                .createQuery("SELECT u FROM User u").getResultList();
    }
}
