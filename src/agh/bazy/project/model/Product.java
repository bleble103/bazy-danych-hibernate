package agh.bazy.project.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="Products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column
    public String ProductName;

    @Column
    public Integer UnitsOnStock;

    @ManyToOne
    public Supplier supplier;

    @ManyToOne
    public Category category;


    public Product() {

    }

    @Override
    public String toString(){
        return "ID: "+id+", " + ProductName+", Units: "+UnitsOnStock;
    }
}
