package agh.bazy.project.model;

import javax.persistence.*;

@Entity
public class InvoiceSlot {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    public int id;

    @ManyToOne
    public Product Product;

    @Column
    public int Quantity;

    @ManyToOne
    public Invoice Invoice;

    public InvoiceSlot() {}
    public InvoiceSlot(Product product, Invoice invoice, int quantity){
        this.Product = product;
        this.Invoice = invoice;
        this.Quantity = quantity;
    }
}
