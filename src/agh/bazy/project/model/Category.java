package agh.bazy.project.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Category {
    public Category() {};
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int CategoryID;

    @Column
    public String Name;

    @OneToMany(mappedBy = "category")
    public Set<Product> Products;

    @Override
    public String toString(){
        return "ID: "+CategoryID+" "+Name;
    }
}
