package agh.bazy.project.model;

import agh.bazy.project.util.UserType;

import javax.persistence.*;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @Column(length = 30)
    public String Username;

    @Column(length = 30)
    public String Password;

    @Column
    public UserType UserType;

    @OneToOne(mappedBy = "User")
    public Company Company;


    public User() {}
}
