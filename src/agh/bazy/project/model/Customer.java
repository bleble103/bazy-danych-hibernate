package agh.bazy.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Customer extends Company {

    @Column
    public double discount;

    @OneToMany (mappedBy = "Customer")
    public Set<Invoice> Invoices;

    public Customer() {}
}
