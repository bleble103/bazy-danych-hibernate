package agh.bazy.project.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Invoices")
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int InvoiceNumber;

    @OneToMany(mappedBy = "Invoice", cascade = CascadeType.PERSIST)
    public Set<InvoiceSlot> InvoiceSlots;

    @ManyToOne
    public Customer Customer;

    public Invoice() {}
}

