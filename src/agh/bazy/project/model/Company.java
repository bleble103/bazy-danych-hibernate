package agh.bazy.project.model;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int CompanyID;

    @Column
    public String CompanyName;

    @Column
    public String Street;

    @Column
    public String City;

    @Column
    public String ZipCode;

    @OneToOne
    public User User;

    public Company() {}
}
