package agh.bazy.project.model;

import javax.persistence.*;

@Embeddable
public class Address {

    @Column
    public String Street;

    @Column
    public String City;

    public Address() {}
}
