package agh.bazy.project.model;

import javax.persistence.*;

@Entity
public class Supplier extends Company{
    @Column
    public String BankAccountNumber;

    public Supplier() {}

    @Override public String toString(){
        return "Dostawca: ID: "+CompanyID+" | "+CompanyName;
    }
}
