package agh.bazy.project;

import agh.bazy.project.dao.CustomerDao;
import agh.bazy.project.dao.SupplierDao;
import agh.bazy.project.dao.UserDao;
import agh.bazy.project.model.Customer;
import agh.bazy.project.model.Supplier;
import agh.bazy.project.model.User;
import agh.bazy.project.util.AuthorizationManager;
import agh.bazy.project.util.SessionManager;
import agh.bazy.project.util.UserType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class Main {
    private static boolean addSupplier(){
        SupplierDao supplierDao = new SupplierDao();
        Scanner inputScanner = new Scanner(System.in);
        System.out.println("username: ");
        String username = inputScanner.nextLine();
        System.out.println("password: ");
        String password = inputScanner.nextLine();
        System.out.println("Company name: ");
        String companyName = inputScanner.nextLine();
        System.out.println("City: ");
        String city = inputScanner.nextLine();
        System.out.println("Street: ");
        String street = inputScanner.nextLine();
        System.out.println("ZipCode: ");
        String zipCode = inputScanner.nextLine();
        System.out.println("Bank Account Number: ");
        String accountNumber = inputScanner.nextLine();
        return supplierDao.createSupplier(username,password,companyName,city,street,zipCode,accountNumber).isPresent();
    }
    private static boolean addCustomer(){
        CustomerDao customerDao = new CustomerDao();
        Scanner inputScanner = new Scanner(System.in);
        System.out.println("username: ");
        String username = inputScanner.nextLine();
        System.out.println("password: ");
        String password = inputScanner.nextLine();
        System.out.println("Company name: ");
        String companyName = inputScanner.nextLine();
        System.out.println("City: ");
        String city = inputScanner.nextLine();
        System.out.println("Street: ");
        String street = inputScanner.nextLine();
        System.out.println("ZipCode: ");
        String zipCode = inputScanner.nextLine();
        System.out.println("Discount (%): ");
        double discount = inputScanner.nextDouble();
        inputScanner.nextLine();

        Optional<Customer> customer = customerDao.createCustomer(username,password,companyName,city,street,zipCode,discount);
        return customer.isPresent();
    }
    //private static final SessionFactory sessionFactory = new Configuration().configure()
    //        .buildSessionFactory();
//    private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabaseConfig");
//    private static final EntityManager entityManager = entityManagerFactory.createEntityManager();
    public static void main(String[] args){
        Scanner inputScanner = new Scanner(System.in);
        UserDao userDao = new UserDao();
        //userDao.createUser("admin", "admin", UserType.ADMIN, null);

        SupplierDao supplierDao = new SupplierDao();
        CustomerDao customerDao = new CustomerDao();
//        supplierDao.createSupplier("user", "passwd", "Dostawca",
//                "Krakow","Uliczna","31-331","111-222-333");
//        supplierDao.createSupplier("usera", "passwda", "Dostawca 2",
//                "Warszawa","Zielona","11-331","22-22332-31133");
//        customerDao.createCustomer("cust", "cust1", "Dostawca 2",
//                "Warszawa","Zielona","11-331",0.1);

//        List<Supplier> suppliers = supplierDao.getAllSuppliers();
//        for(Supplier s : suppliers){
//            System.out.println(s.User.Username);
//        }

        while(true) {
            System.out.println("login | add customer | add supplier: ");
            String line = inputScanner.nextLine();
            if (line.equals("login")){
                break;
            }
            if(line.equals("add customer")){

                if(addCustomer()) System.out.println("Customer added");
                else System.out.println("Cannot add customer");
            }
            if(line.equals("add supplier")){
                if(addSupplier()) System.out.println("Supplier added");
                else System.out.println("Cannot add supplier");
            }
            else{
                System.out.println("Wrong command");
            }
        }
        AuthorizationManager.login();
        while(true){
            SessionManager.nextCommand();
        }


        //Session session = sessionFactory.openSession();
        //Transaction t = session.beginTransaction();
//        EntityTransaction etx = entityManager.getTransaction();
//        etx.begin();
//        etx.commit();
//        entityManager.close();



        /*
        Supplier supplier = new Supplier();
        supplier.City = "Krakow"; supplier.Street ="Uliczna"; supplier.CompanyName = "Dostawca";
        supplier.ZipCode="31-331"; supplier.BankAccountNumber="1111-4444-3333";

        Customer customer = new Customer();
        customer.City = "Warszawa"; customer.CompanyName="Klient"; customer.Street ="Sezamkowa";
        customer.ZipCode = "01-221"; customer.discount = 0.1;
        entityManager.persist(supplier); entityManager.persist(customer);

        Supplier dbSupplier = (Supplier) entityManager
                .createQuery("SELECT s FROM Supplier s WHERE s.CompanyName = 'Dostawca'")
                .getSingleResult();
        System.out.println(dbSupplier.BankAccountNumber);

        Customer dbCustomer = (Customer) entityManager
                .createQuery("SELECT c FROM Customer c WHERE c.CompanyName = 'Klient'")
                .getSingleResult();
        System.out.println(dbCustomer.discount);

        etx.commit();
        entityManager.close();


        Supplier s = new Supplier();
        s.CompanyName = compName;
        s.Street = street;
        s.City = city;

        entityManager.persist(s);

        Invoice invoice = new Invoice();
        invoice.Products = new HashSet<>();
        Product komputer = new Product();
        komputer.Invoices = new HashSet<>();
        komputer.ProductName = "Komputer";
        komputer.UnitsOnStock = 10;
        Product klawiatura = new Product();
        klawiatura.Invoices = new HashSet<>();
        klawiatura.ProductName = "Klawiatura";
        klawiatura.UnitsOnStock = 5;
        invoice.Quantity = 2;
        invoice.Products.add(komputer);
        invoice.Products.add(klawiatura);
        komputer.Invoices.add(invoice);
        klawiatura.Invoices.add(invoice);

        entityManager.persist(komputer);
        entityManager.persist(klawiatura);

        etx.commit();
        entityManager.close();


        Product baklazan = session.get(Product.class, 1);
        Product marchew = session.get(Product.class, 2);
        Product ogorek = session.get(Product.class, 3);

        Invoice i1 = new Invoice();
        i1.Products = new HashSet<>();
        i1.Quantity = 2;

        i1.Products.add(baklazan);
        baklazan.Invoices.add(i1);

        i1.Products.add(marchew);
        marchew.Invoices.add(i1);

        session.save(i1);
        session.save(baklazan);
        session.save(marchew);

        Invoice i2 = new Invoice();
        i2.Products = new HashSet<>();
        i2.Quantity = 1;
        i2.Products.add(ogorek);
        ogorek.Invoices.add(i2);
        i2.Products.add(marchew);
        marchew.Invoices.add(i2);
        session.save(i2);
        session.save(ogorek);
        session.save(marchew);

        System.out.println("Produkty sprzedane w ramach faktury nr "+i1.InvoiceNumber);
        for(Product pr : i1.Products){
            System.out.println("\t "+pr.ProductName);
        }

        System.out.println("Produkt " + marchew.ProductName+" sprzedany na fakturach nr:");
        for(Invoice inv : marchew.Invoices){
            System.out.println("\t "+inv.InvoiceNumber);
        }

        System.out.println("Podaj nazwę dostawcy: ");
        String compName = inputScanner.nextLine();

        System.out.println("Podaj ulice dostawcy: ");
        String street = inputScanner.nextLine();


        System.out.println("Podaj miasto dostawcy: ");
        String city = inputScanner.nextLine();

        Supplier s = new Supplier();
        s.CompanyName = compName;
        s.Street = street;
        s.City = city;
        s.Products = new HashSet<>();
        entityManager.persist(s);
        //session.save(s);
        Category c = null;
        for(int i=0; i<2; i++){
            c = new Category();
            System.out.println("Podaj nazwę kategorii: ");
            c.Name = inputScanner.nextLine();
            c.Products = new HashSet<>();
            entityManager.persist(c);
            //session.save(c);
        }
        for(int i=0; i<5; i++){
            Product p = new Product();
            System.out.println("Podaj nazwę produktu: ");
            p.ProductName = inputScanner.nextLine();
            System.out.println("Podaj ilość produktu: ");
            p.UnitsOnStock = inputScanner.nextInt();
            inputScanner.nextLine();

            p.supplier = s;
            //session.save(p);
            entityManager.persist(p);
            s.Products.add(p);
        }
        //session.save(s);
        entityManager.persist(s);
        etx.commit();
        //t.commit();
        etx = entityManager.getTransaction();
        etx.begin();
        //t = session.beginTransaction();

        for(int i=1; i<= 3; i++){
            //Product p = session.get(Product.class, i);
            Product p = entityManager.find(Product.class, i);
            p.category = c;
            System.out.println(c.Name);
            c.Products.add(p);
            entityManager.persist(p);
            entityManager.persist(c);
            //session.save(p);
            //session.save(c);
        }


        Set<Product> products = c.Products;
        System.out.println("Produkty w kategorii " + c.Name);
        for(Product pr : products){
            System.out.println("\t" + pr.ProductName);
        }

        //Product pr = session.get(Product.class, 2);
        Product pr = entityManager.find(Product.class, 2);
        System.out.println("\nProdukt "+pr.ProductName+" należy do kategorii " + pr.category.Name);
        etx.commit();

        //t.commit();
        //session.close();


        //sessionFactory.close();

         */
    }
}
