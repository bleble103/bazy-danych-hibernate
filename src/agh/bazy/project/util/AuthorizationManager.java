package agh.bazy.project.util;

import agh.bazy.project.dao.CustomerDao;
import agh.bazy.project.dao.UserDao;
import agh.bazy.project.model.User;

import java.util.Optional;
import java.util.Scanner;

public class AuthorizationManager {
    private static boolean auth(String login, String password){
        UserDao userDao = new UserDao();
        CustomerDao customerDao = new CustomerDao();
        Optional<User> user = userDao.findByUsername(login);
        if(user.isPresent()){
            if(user.get().Password.equals(password)){
                SessionManager.userType = user.get().UserType;
                if(SessionManager.userType == UserType.CUSTOMER){
                    SessionManager.currentCustomer = customerDao.getByLogin(login);
                }
                return true;
            }
        }
        return false;
    }
    public static void login(){

        while(true) {
            Scanner inputScanner = new Scanner(System.in);
            System.out.println("login: ");
            String log = inputScanner.nextLine();
            System.out.println("password: ");
            String pswd = inputScanner.nextLine();

            if(auth(log, pswd)){
                System.out.println("Logged in");
                break;
            }else{
                System.out.println("Wrong username or password");
            }
        }

    }
}
