package agh.bazy.project.util;

import agh.bazy.project.dao.CategoryDao;
import agh.bazy.project.dao.InvoiceDao;
import agh.bazy.project.dao.ProductDao;
import agh.bazy.project.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class SessionManager {

    public static UserType userType;
    public static final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
    public static Session session = null;
    public static List<InvoiceSlot> cart = new ArrayList<>();
    public static Customer currentCustomer = null;

    public static Session getSession(){
        if(session == null) session = sessionFactory.openSession();
        return session;
    }

    public static void closeSession(){
        session.close();
        session = null;
    }

    public static void closeSessionFactory(){
        sessionFactory.close();
    }

    private static void showCommands(){
        System.out.print("categories | products");
        if(userType == UserType.CUSTOMER){
            System.out.print(" | product | cart | order | orders");
        }
        if(userType == UserType.ADMIN){
            System.out.print(" | addcategory | addproduct");
        }

        System.out.print(" | exit\n");
    }

    private static void listCategories(){
        CategoryDao categoryDao = new CategoryDao();
        List<Category> categories = categoryDao.getAllCategories();
        for(Category c: categories){
            System.out.println(c);
        }
    }
    public static void nextCommand(){

        CategoryDao categoryDao = new CategoryDao();
        ProductDao productDao = new ProductDao();
        InvoiceDao invoiceDao = new InvoiceDao();
        Scanner inputScanner = new Scanner(System.in);
        showCommands();
        String command = inputScanner.nextLine();

        if(command.equals("categories")){
            listCategories();
        }
        if(command.equals("products")){
            listCategories();
            System.out.println("Which category <ID>: ");
            int id = inputScanner.nextInt();
            inputScanner.nextLine();
            Optional<Category> category = categoryDao.getById(id);
            if(category.isPresent()) {
                for(Product p: category.get().Products){
                    System.out.println(p);
                }
            }
        }
        if(command.equals("addcategory")){
            System.out.println("Category name: ");
            String categoryName = inputScanner.nextLine();
            if(categoryDao.createCategory(categoryName).isPresent()){
                System.out.println("Category created");
            }
            else{
                System.out.println("Cannot create category");
            }
        }
        if(command.equals("addproduct")){
            listCategories();
            System.out.println("Which category <ID>: ");
            int id = inputScanner.nextInt();
            inputScanner.nextLine();
            Optional<Category> category = categoryDao.getById(id);
            if(category.isPresent()){
                System.out.println("Product name: ");
                String productName = inputScanner.nextLine();
                System.out.println("Units in stock: ");
                int units = inputScanner.nextInt();
                inputScanner.nextLine();
                if(productDao.createProduct(productName,category.get(),units).isPresent()){
                    System.out.println("Product added");
                }
                else{
                    System.out.println("Cannot add product");
                }

            }
        }
        if(command.equals("product")){

            listCategories();
            System.out.println("Which category <ID>: ");
            int id = inputScanner.nextInt();
            inputScanner.nextLine();
            Optional<Category> category = categoryDao.getById(id);
            if(category.isPresent()) {
                for(Product p:category.get().Products){
                    System.out.println(p);
                }
                System.out.println("Which product <ID>: ");
                int pid = inputScanner.nextInt();
                inputScanner.nextLine();
                Optional<Product> product = productDao.getById(pid);
                if (product.isEmpty()) {
                    System.out.println("Wrong product");
                    return;
                }
                System.out.println("Quantity: ");
                int quantity = inputScanner.nextInt();
                if(quantity > product.get().UnitsOnStock){
                    System.out.println("Too many units");
                    return;
                }
                inputScanner.nextLine();
                InvoiceSlot invoiceSlot = new InvoiceSlot();
                invoiceSlot.Product = product.get();
                invoiceSlot.Quantity = quantity;
                cart.add(invoiceSlot);
            }else{
                System.out.println("Wrong category");
            }
        }
        if(command.equals("cart")){
            for(InvoiceSlot is:cart){
                System.out.println(is.Product.ProductName+" x "+is.Quantity);
            }
        }
        if(command.equals("order")){
            invoiceDao.createInvoice(currentCustomer, cart);
            cart.clear();
            System.out.println("Order made");
        }
        if(command.equals("orders")){
            for(Invoice inv : currentCustomer.Invoices){
                System.out.println("ID: "+inv.InvoiceNumber);
            }
            System.out.println("Which invoice <ID>: ");
            int id = inputScanner.nextInt();
            inputScanner.nextLine();
            for(Invoice inv : currentCustomer.Invoices){
                if(inv.InvoiceNumber == id){
                    for(InvoiceSlot invs : inv.InvoiceSlots){
                        System.out.println(invs.Product.ProductName+" x "+invs.Quantity);
                    }
                    break;
                }
            }
        }
        if(command.equals("exit")){
            SessionManager.closeSession();
            SessionManager.closeSessionFactory();
            System.exit(0);
        }
    }

}
